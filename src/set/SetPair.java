/**************************************************
 * Author : Ludovic TOISON
 * Heriot Watt University, Computer Science year 3.
 * Project : Foundations 2 - PART 2
 **************************************************/

package set;
import java.util.*;

public class SetPair implements SetTree{

	private ArrayList<SetTree> set;
	
	public SetPair (){
		this.set=new ArrayList<SetTree>();
	}
	
	public SetPair (ArrayList<SetTree> e){
		if(e.size()<3){
			this.set=e;
		}
	}

	public SetPair (SetTree x, SetTree y){
		this.set=new ArrayList<SetTree>();
		this.add(x);
		this.add(y);
	}

	public void add(SetTree e){		
		if(this.set.size()<2){
			this.set.add(e);
		}		
	}
	
	public void display(){
		Iterator<SetTree> ite = this.set.iterator();
		System.out.print("(");
		while(ite.hasNext() && this.set.size()<3){
			SetTree value = ite.next();
			value.display();
			if(ite.hasNext()){
				System.out.print(",");
			}
		}
		System.out.print(")");
	}

	public String print(){
		String tmp = "";
		Iterator<SetTree> ite = this.set.iterator();
		tmp = tmp+"(";
		while(ite.hasNext() && this.set.size()<3){
			SetTree value = ite.next();
			tmp = tmp+value.print();
			if(ite.hasNext()){
				tmp = tmp+",";
			}
		}
		tmp = tmp+")";
		return tmp;
	}
	
	public SetTree getSet() {
		return this;
	}
	
	public ArrayList<SetTree> getSetArray(){
		return this.set;
	}
	
	public SetTree getValue(int pos){
		if(pos<2){
			return this.set.get(pos);
		}else{
			return null;
		}
		
	}
	
	public boolean equals(Object o) {
		if (o instanceof SetPair && ((SetPair)o).getSetArray().size()==this.set.size()) {
			int i = 0;
			Iterator<SetTree> ite = ((SetPair)o).getSetArray().iterator();
			Iterator<SetTree> ite2 = this.set.iterator();
			while(ite.hasNext()){
				while(ite2.hasNext()){
					if(ite.next().equals(ite2.next())){
						i++;
					}
				}
			}
			
			if(i == this.set.size()){
				return true;
			}else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	public boolean contains(Object e) {
		boolean contains = false;
		Iterator<SetTree> ite = this.set.iterator();
		while(ite.hasNext()){
			if(ite.next().equals(e)){
				contains = true;
			}
		}
		return contains;
	}
}
