/**************************************************
 * Author : Ludovic TOISON
 * Heriot Watt University, Computer Science year 3.
 * Project : Foundations 2 - PART 2
 **************************************************/

package set;

public class SetLeaf implements SetTree{
	
	private int value;
	
	public SetLeaf(int x) {
		this.value=x;
	}
	
	public void add(SetTree e){
		
	}
	
	public boolean contains(Object e) {
		if(e instanceof Integer && e=="value"){
			return true;
		}else{
			return false;
		}
	}
	
	public String toString(){
		return this.value+"";
	}

	public void display(){
		System.out.print(value);
	}
	
	public String print(){
		return this.toString();
	}
	
	public SetTree getSet() {		
		return this;
	}

	public int getValue(){
		return this.value;
	}
	
	public boolean equals(Object o) {
		if (o instanceof SetLeaf && this.value ==((SetLeaf)o).getValue()) {
			return true;
		}else{
			return false;
		}
	}

}
