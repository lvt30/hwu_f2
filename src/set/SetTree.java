/**************************************************
 * Author : Ludovic TOISON
 * Heriot Watt University, Computer Science year 3.
 * Project : Foundations 2 - PART 2
 **************************************************/

package set;

public interface SetTree {
	public abstract SetTree getSet();
	public abstract void display();
	public String print();
	public boolean equals(Object o);
	public abstract void add(SetTree element);
	public boolean contains(Object e);
}
