/**************************************************
 * Author : Ludovic TOISON
 * Heriot Watt University, Computer Science year 3.
 * Project : Foundations 2 - PART 2
 **************************************************/

package set;
import java.util.*;

public class SetNode implements SetTree{

	private ArrayList<SetTree> set;
	
	public SetNode (){
		this.set=new ArrayList<SetTree>();
	}
	
	public SetNode (SetTree e){
		this.set=new ArrayList<SetTree>();
		this.set.add(e);
	}
	
	public SetNode (ArrayList<SetTree> e){
		this.set=e;
	}

	public void add(SetTree e){
		if(!this.contains(e)){
			this.set.add(e);
		}
	}
	
	public void display(){
		Iterator<SetTree> ite = this.set.iterator();
		System.out.print("{");
		while(ite.hasNext()){
			SetTree value = ite.next();
			value.display();
			if(ite.hasNext()){
				System.out.print(",");
			}
		}
		System.out.print("}");
	}
	
	public String print(){
		String tmp = "";
		Iterator<SetTree> ite = this.set.iterator();
		tmp = tmp+"{";
		while(ite.hasNext()){
			SetTree value = ite.next();
			tmp = tmp+value.print();;
			if(ite.hasNext()){
				tmp = tmp+",";
			}
		}
		tmp = tmp+"}";
		return tmp;
	}

	public SetTree getSet() {
		return this;
	}
	
	public ArrayList<SetTree> getSetArray(){
		return this.set;
	}

	public boolean equals(Object o) {
		if (o instanceof SetNode && ((SetNode)o).getSetArray().size()==this.set.size()) {
			int i = 0;
			Iterator<SetTree> ite = ((SetNode)o).getSetArray().iterator();
			Iterator<SetTree> ite2 = this.set.iterator();
			while(ite.hasNext()){
				while(ite2.hasNext()){
					if(ite.next().equals(ite2.next())){
						i++;
					}
				}
			}		
			if(i == this.set.size()){
				return true;
			}else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	public boolean contains(Object e){
		boolean find = false;
		Iterator<SetTree> ite = this.set.iterator();
			while (ite.hasNext() && !find){
				if(ite.next().equals(e)){
					find = true;
				}
			}			
		return find;
	}

	public SetNode difference(SetNode x) {		
		SetNode tmp = new SetNode();
		Iterator<SetTree> ite = this.set.iterator();
		while(ite.hasNext()){
			SetTree value = ite.next();
			if (!x.contains(value)){		
				tmp.add(value);
			}
		}
		return tmp;
	}
		
	public SetNode intersection(SetNode x) {
		SetNode tmp = new SetNode();
		Iterator<SetTree> ite = x.getSetArray().iterator();
		while(ite.hasNext()){
			SetTree value = ite.next();	
			if (this.set.contains((value))){		
					tmp.add(value);
				}
			}
			return tmp;
	}
	
	public SetNode union(SetNode e) {					
		SetNode tmp = new SetNode();
		Iterator<SetTree> ite = this.set.iterator();
		while(ite.hasNext()){
			tmp.add(ite.next());
		}
		Iterator<SetTree> ite2 = e.getSetArray().iterator();
		while(ite2.hasNext()){
			tmp.add(ite2.next());
		}
		return tmp;
	}
	
	public boolean isFunction(){
		boolean ret = true;
		ArrayList<SetTree> key = new ArrayList<SetTree>();
		
		Iterator<SetTree> ite = this.set.iterator();
			while (ite.hasNext() && ret){
				SetTree o = ite.next();
				if(o instanceof SetPair){
					SetPair tmp = (SetPair)o;
					if(!key.contains(tmp.getValue(0))){
						key.add(tmp.getValue(0));
					}else{
						ret = false;
					}
				}else{
					ret = false;
				}
			}			
		return ret;
	}
	
	public boolean isInjective(){
		boolean ret = true;
		ArrayList<SetTree> key = new ArrayList<SetTree>();
		ArrayList<SetTree> value = new ArrayList<SetTree>();
		
		Iterator<SetTree> ite = this.set.iterator();
			while (ite.hasNext() && ret){
				SetTree o = ite.next();
				if(o instanceof SetPair && ret){
					SetPair tmp = (SetPair)o;
					if(!key.contains(tmp.getValue(0)) && !value.contains(tmp.getValue(1))){
						key.add(tmp.getValue(0));
						value.add(tmp.getValue(1));	
					}else{
						ret = false;
					}
				}else{
					ret = false;
				}
			}			
		return ret;
	}

	public SetTree applyFunction(SetTree input){
		Iterator<SetTree> ite = this.set.iterator();
			while (ite.hasNext()){
				SetTree o = ite.next();
				if(o instanceof SetPair){
					SetPair tmp = (SetPair)o;
					if(tmp.getValue(0).equals(input)){
						return tmp.getValue(1);
					}
				}			
			}
		return null;
	}
	
	public SetNode dom(){
		SetNode dom = new SetNode();
		Iterator<SetTree> ite = this.set.iterator();
		while (ite.hasNext()){
			SetTree o = ite.next();
			if(o instanceof SetPair){
				SetPair tmp = (SetPair)o;
				if(!dom.contains(tmp.getValue(0))){
					dom.add(tmp.getValue(0));
				}
			}
		}
		return dom;
	}
	
	public SetNode ran(){
		SetNode ran = new SetNode();
		Iterator<SetTree> ite = this.set.iterator();
		while (ite.hasNext()){
			SetTree o = ite.next();
			if(o instanceof SetPair){
				SetPair tmp = (SetPair)o;
				if(!ran.contains(tmp.getValue(1))){
					ran.add(tmp.getValue(1));
				}
			}
		}
		return ran;
	}
	
	public SetNode inverse(){
		SetNode inv = new SetNode();
		
		Iterator<SetTree> ite = this.set.iterator();
		while (ite.hasNext()){
			SetTree o = ite.next();
			if(o instanceof SetPair){
				SetPair tmp = (SetPair)o;
				SetPair x = new SetPair(tmp.getValue(1),tmp.getValue(0));
				if(!inv.contains(x)){
					inv.add(x);
				}
			}
		}	
		return inv;
	}

}