/**************************************************
 * Author : Ludovic TOISON
 * Heriot Watt University, Computer Science year 3.
 * Project : Foundations 2 - PART 2
 **************************************************/

import set.*;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Iterator;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Application {
	 	
	private static String INPUT_FILENAME = "/home/ludo/workspace/Foundations2/src/input.json"; // Must be update according to the current location of the file			
	private static String OUTPUT_FILENAME = "output.txt";
	private static SetTree[] expressions;
	private static String[] variables;
	
	/* Function : Read the Jsonfile and build an array of SetTree (each element is an expression).
	 * Parameter : Jsonfile
	 * Return : Array of SetTree */
	private static SetTree[] read(JsonObject input){
		/* Formating the file to get the expressions */
		JsonArray statements = (JsonArray) input.get("statement-list");
		expressions = new SetTree[statements.size()];
		variables = new String[statements.size()];
		int i = 0;
		
		/* Reading every expression */
		Iterator<JsonElement> ite = statements.iterator();
		while(ite.hasNext()){
			JsonObject expression = (JsonObject)ite.next();
			/* Formating the expression */
			if(expression.has("arguments") && ((JsonArray) expression.get("arguments")).size()==2){
				JsonArray arguments = (JsonArray) expression.get("arguments");
				
				if(((JsonObject) arguments.get(0)).get("variable")!=null){
					String variable = ((JsonObject) arguments.get(0)).get("variable").getAsString(); // Name of the variable	 
					JsonElement value = (JsonElement) arguments.get(1); // Value of the variable
				
					/* Generate the SetTree using our structure */
					if(value.isJsonPrimitive()){
						/* If the value is a primitive : it is an int
						 * so we save it in our array */
						expressions[i] = new SetLeaf(value.getAsInt());	
					}else if(value.isJsonObject()){
						/* Else if it is a structure : it is a set or tuple 
						 * so we go through the structure */
						expressions[i] = generate((JsonObject)value); 	
					}else{
						/* Else it is undefined */
						System.out.println("BAD INPUT : no value is defined !");
						expressions[i] = null;
					}
					variables[i] = variable; // Put the name of the variable in our current expression
				}else{
					System.out.println("BAD INPUT : no variable is defined !");
					expressions[i] = null;
				}
			}else{
				System.out.println("BAD INPUT : not enough arguments are defined !");
				expressions[i] = null;
			}
			i++;
		}
		return expressions;
	}

	/* Function : Look for the SetTree corresponding to the variable.
	 * Parameter : Name of the variable (String)
	 * Return : The appropriate record in the array of SetTree or null */
	private static SetTree getSetTree(String variable){
		SetTree tmp = null;
		for(int i = 0; i<expressions.length; i++){
			if(expressions[i]!=null && variables[i].equals(variable)){
				tmp = expressions[i];
			}
		}
		return tmp;
	}
	
	/* Function : Generate a complex expression.
	 * Parameter : Subexpression which is a variable, a structure or an operation
	 * Return : SetTree */
	private static SetTree generate(JsonObject value){
		SetTree tmp = null;	
		
		if(value.has("operator")){
			/* If it is a subexpression : formating the subexpression */
			String operator = value.get("operator").getAsString();
			JsonArray arguments = value.get("arguments").getAsJsonArray();
		
			if(operator.equals("set") || operator.equals("tuple")){
				/* If it is a structure : we initialise the appropriate objects*/	
				if(operator.equals("set")){
					tmp = new SetNode();
				}else{
					tmp = new SetPair();
				}
				/* We read the arguments */
				Iterator<JsonElement> ite = arguments.iterator();
				while(ite.hasNext()){
					JsonElement term = (JsonElement) ite.next();
					if(term.isJsonPrimitive()){
						/* If the value is a primitive : it is an int
						 * so we save it in our object */
						tmp.add(new SetLeaf(term.getAsInt()));
					}else{
						/* Else it is a subexpression 
						 * so we go through it */
						JsonObject subexpression = term.getAsJsonObject();
						tmp.add(generate(subexpression));			
					}		
				}
			}else if (operator.equals("equal")){	
				/* If it is an operation */
				if(arguments.size()!=2){
					System.out.println("To evaluate equality, you must have only 2 arguments");
					return null;
				}
				/* We use a tmp array of SetTree to compare them (membership or equality) */
				JsonObject[] term = new JsonObject[2];
				SetTree[] terms = new SetTree[2];
				for(int i = 0; i < 2; i++){
					term[i] = arguments.get(i).getAsJsonObject();
					terms[i] = generate(term[i].getAsJsonObject());
				}
				if(terms[1].equals(terms[0])){
					tmp = new SetLeaf(1);
				}else{
					tmp = new SetLeaf(0);
				}
			}else if (operator.equals("member")){	
				/* If it is an operation */
				if(arguments.size()!=2){
					System.out.println("To evaluate membership, you must have only 2 arguments");
					return null;
				}
				/* We use a tmp array of SetTree to compare them (membership or equality) */
				JsonObject[] term = new JsonObject[2];
				SetTree[] terms = new SetTree[2];
				for(int i = 0; i < 2; i++){
					term[i] = arguments.get(i).getAsJsonObject();
					terms[i] = generate(term[i].getAsJsonObject());
				}
				if(terms[1].contains(terms[0])){
					tmp = new SetLeaf(1);
				}else{
					tmp = new SetLeaf(0);
				}
			}else if (operator.equals("is-function")){
				if(arguments.size()!=1){
					System.out.println("To evaluate a function, you must have only 1 argument");
					return null;
				}
				/* We temporary element to build the appropriate element (set, pair or leaf) */
				JsonObject element = arguments.get(0).getAsJsonObject();
				SetTree subexp = generate(element.getAsJsonObject());
				
				if(subexp instanceof SetNode && (((SetNode)subexp).isFunction())){
					/* To be a function it has to be a set structure */
					tmp = new SetLeaf(1);
				}else{
					tmp = new SetLeaf(0);
				}
			}else if (operator.equals("is-injective")){
				if(arguments.size()!=1){
					System.out.println("To evaluate a function, you must have only 1 argument");
					return null;
				}
				/* We temporary element to build the appropriate element (set, pair or leaf) */
				JsonObject element = arguments.get(0).getAsJsonObject();
				SetTree subexp = generate(element.getAsJsonObject());
				
				if(subexp instanceof SetNode && (((SetNode)subexp).isInjective())){
					/* To be a function it has to be a set structure */
					tmp = new SetLeaf(1);
				}else{
					tmp = new SetLeaf(0);
				}
			}else if (operator.equals("apply-function")){
				if(arguments.size()!=2){
					System.out.println("To apply a function, you must have only 2 arguments");
					return null;
				}
				
				SetTree func = generate(arguments.get(0).getAsJsonObject());
				SetTree x = null;
				
				if(arguments.get(1).isJsonPrimitive()){
					/* If the argument (parameter) is an int we get back it directly */
					x = new SetLeaf(arguments.get(1).getAsInt());
				}else if(arguments.get(1).isJsonObject()){
					/* else we build the appropriate structure (set, pair or leaf) */
					x = generate(arguments.get(1).getAsJsonObject());
				}
				
				if(func instanceof SetNode && ((SetNode)func).isFunction()){
					tmp = ((SetNode)func).applyFunction(x);
				}else{
					System.out.println("The first argument as to be a function");
					return null;
				}
				
			}else if (operator.equals("domain") || (operator.equals("range"))){
				if(arguments.size()!=1){
					System.out.println("To evaluate a relation, you must have only 1 argument");
					return null;
				}
				JsonObject element = arguments.get(0).getAsJsonObject();
				SetTree subexp = generate(element.getAsJsonObject());
				
				if(subexp instanceof SetNode && operator.equals("domain")){
					/* according to the operator we call the appropriate function*/
					tmp = ((SetNode)subexp).dom();
				}else if (subexp instanceof SetNode && operator.equals("range")){
					tmp = ((SetNode)subexp).ran();
				}else{
					System.out.println("To evaluate the domain or the range, the argument has to be a set");
					return null;
				}
			}else if(operator.equals("intersection") || (operator.equals("union")) || (operator.equals("set-difference"))){
				if(arguments.size()!=2){
					System.out.println("To execute intersection, union or substraction, you must have only 2 arguments");
					return null;
				}
				/* We use a tmp array of SetTree to build union, difference and intersection operation */
				JsonObject[] term = new JsonObject[2];
				SetTree[] terms = new SetTree[2];
				for(int i = 0; i < 2; i++){
					term[i] = arguments.get(i).getAsJsonObject();
					terms[i] = generate(term[i].getAsJsonObject());
				}
				if(terms[0] instanceof SetNode && terms[1] instanceof SetNode){
					if(operator.equals("intersection")){
						tmp = ((SetNode)terms[0]).intersection((SetNode)terms[1]);
					}else if(operator.equals("union")){
						tmp = ((SetNode)terms[0]).union((SetNode)terms[1]);
					}else if(operator.equals("set-difference")){
						tmp = ((SetNode)terms[0]).difference((SetNode)terms[1]);
					}
				}else{
					System.out.println("To execute intersection, union or substraction, the 2 arguments have to be sets");
					return null;
				}
				
			}else if(operator.equals("inverse")){
				if(arguments.size()!=1){
					System.out.println("To execute inverse, you must have only 1 argument");
					return null;
				}
				JsonObject element = arguments.get(0).getAsJsonObject();
				SetTree subexp = generate(element.getAsJsonObject());
				
				if(subexp instanceof SetNode){
					tmp = ((SetNode) subexp).inverse();
				}else{
					System.out.println("To execute execute inverse, the argument has to be a set");
					return null;
				}
			}
			
		}else{
			/* Else it is a variable so we just return it */
			String variable = value.get("variable").getAsString();
			tmp = getSetTree(variable);
		}
		return tmp;
	}
	
	/* Function : Display every expression of the array of SetTree. */
	public static void display(){
		for (int i=0;i<expressions.length;i++){
			if(expressions[i]!=null){
				System.out.print(variables[i]+" = ");
				expressions[i].display();
				System.out.println(" ;");
			}else{
				System.out.println(variables[i]+" = Undefined ! ;");
			}
		}
	}
	
	/* Function : Print every expression of the array of SetTree. 
	 * Return : String */
	public static String print(){
		String tmp = "";
		for (int i=0;i<expressions.length;i++){
			if(expressions[i]!=null){
				tmp = tmp+variables[i]+" = "+expressions[i].print()+" ;\n";
			}else{
				tmp = tmp+variables[i]+"Undefined"+" ;\n";
			}
		}
		return tmp;
	}
	
	public static void main(String[] args) throws Exception {
		JsonParser parser = new JsonParser();
		try {
  			JsonObject input = (JsonObject) parser.parse(new FileReader(INPUT_FILENAME));
  			read(input);
  			try{
  				BufferedWriter output = new BufferedWriter(new FileWriter(OUTPUT_FILENAME));
  				output.write(print());
  				display();
  				output.close();
  				System.out.println("Results are in output.txt");
  			}catch(Exception e){
  				System.err.println(e.getMessage());
			}
  		} catch(FileNotFoundException e) {
  			System.err.println("The file does not exist : " + e.getMessage());
  		}
	}
}
